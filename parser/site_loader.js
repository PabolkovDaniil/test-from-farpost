const request = require('request');
const iconv = require('iconv-lite');

function siteLoader(url) {
    return new Promise((resolve, reject) => {
        request({
            url,
            encoding: null,
            followAllRedirects: false,
            jar: true,
            headers: {
                'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36',
            }
        }, (error, response, body) => {
            if (/charset="?(windows-?1251|cp-?1251)"?/i.test(body)) {
                body = iconv.decode(body, 'cp1251');
            } else {
                body = iconv.decode(body, 'utf-8');
            }
            if (!error) {
                resolve(body);
            } else {
                console.error('ERR:', error);
                reject(error);
            }
        });
    });
}

module.exports = siteLoader;