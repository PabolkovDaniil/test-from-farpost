const loadSite = require('./site_loader');
const jsdom = require('jsdom');
const { JSDOM } = jsdom;
const moment = require('moment');
const path = require('path');
const fs = require('fs');

moment.locale('ru');

/** @type {URL} url */
async function parsePost(url) {
    console.log(`Качаем и парсим страницу ${url.href}`);
    const postBody = await loadSite(url.href);
    if (postBody instanceof Error) {
        throw postBody;
    }

    const dom = new JSDOM(postBody);
    const document = dom.window.document;

    if (document.title === 'Фарпост — Доска объявлений') {
        throw new Error('Почему-то открылась главная страница? Oo');
    }

    const $postId = document.querySelector('[bulletinid]');

    const $header = document.querySelector('header.viewbull-header');

    const $title = $header.querySelector('.inplace');
    $title.removeChild($title.querySelector('nobr'));

    const $date = $header.querySelector('.viewbull-header__actuality');
    const dateSrcStr = $date.textContent.replace(/\s+/g, ' ');
    const dateStr = dateSrcStr.replace(/сегодня|вчера/gi, (foundSubStr) => {
        const today = moment();
        if (foundSubStr.toLowerCase() == 'вчера') {
            today.subtract(1, 'day')
        }

        return today.format('D MMMM YYYY');
    });
    const date = moment(dateStr, ['HH:mm, D MMMM', 'HH:mm, D MMMM YYYY']);

    const $postBody = document.querySelector('#fieldsetView[bulletinid]');

    // Удаляем все теги script, чтобы js не отобржался в описании
    $postBody.querySelectorAll('script').forEach($el => $el.remove());

    const id = $postId.getAttribute('bulletinid');
    const subject = $title.textContent;
    const text = $postBody.textContent.trim()
        .replace(/ +/gm, ' ')
        .replace(/\t*/gm, '')
        .replace(/\n{2,}/gm, '\n\n')
    ;

    let images = [];
    postBody.toString().replace(/var\s+items\s*=\s*(\[[^\]]+\])/g, (_, found) => {
        images = JSON.parse(found).map(x => x.src);
    });

    const $owner = document.querySelector('.ownerInfoInner');
    const ownerId = $owner.getAttribute('data-id');

    return {
        post: {
            id,
            publishDate: Number(date),
            publishDateString: dateSrcStr,
            bulletinSubject: subject,
            bulletinText: text,
            bulletinImages: images,
            ownerId: ownerId,
            ownerLogin: null,
        },
        ownerProfileLink: new URL(`https://www.farpost.ru/user/${ownerId}`),
    };
}

async function parseProfile(profileLink) {
    const profileBody = await loadSite(profileLink);
    const dom = new JSDOM(profileBody);
    const document = dom.window.document;

    const $profileIdContainer = document.querySelector('[data-view-dir-user-id]');
    const profileId = Number($profileIdContainer.getAttribute('data-view-dir-user-id'));

    const $nickname = document.querySelector('.userNick');
    const nickname = $nickname.textContent.trim();

    return {
        ownerId: profileId,
        ownerLogin: nickname,
    };
}

function saveToFile(data) {
    const filePath = path.resolve(__dirname, `./posts.mock.${Date.now()}.json`);
    fs.writeFileSync(filePath, JSON.stringify(data, null, 2));
}

module.exports = {
    parsePost,
    parseProfile,
    saveToFile,
};
