const fs = require('fs');
const path = require('path');
const siteLoader = require('./site_loader');
const { parsePost, parseProfile, saveToFile } = require('./utils');

console.info('Получаем список ссылок для парсинга (из links.txt)');

const linksFile = fs.readFileSync(path.resolve(__dirname, './links.txt')).toString();
const linksUrl = new Set();
const links = linksFile
    .split(/\n/g)
    .map(link => {
        try {
            const url = new URL(link);
            if (linksUrl.has(url.pathname)) {
                return null;
            }

            linksUrl.add(url.pathname);
            return url;
        } catch (err) {
            console.warn(`"${link}" не является ссылкой`);
            return null;
        }
    })
    .filter(x => Boolean(x))
;

console.log('\n');

(async () => {
    const postsMap = new Map();

    // Загружаем главную страницу, чтобы получить куки.
    //  Без кук на каждый запрос парсинга будет возвращаться главная страница (видимо, без кук), и все запросы упадут.
    //      Сия проблема появилась только запрос на 30-й (образно) :(
    await siteLoader('https://www.farpost.ru/');

    console.info('Парсим страницы:');
    for (let i = 0, il = links.length; i < il; ++i) {
        const link = links[i];
        try {
            const postInfo = await parsePost(link);
            const profileData = await parseProfile(postInfo.ownerProfileLink);

            postsMap.set(postInfo.post.id, {
                ...postInfo.post,
                ...profileData,
            });
        } catch (err) {
            console.error(`Не удалось получить пост по адресу "${link.href}":`, err);
        }
    }
    console.log('\n');

    console.info('Генерируем json мок...');
    const posts = Array.from(postsMap).map(([id, postData]) => postData);

    console.info('Сохранение файла мак...');
    saveToFile(posts);
})();