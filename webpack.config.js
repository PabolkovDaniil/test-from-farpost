const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const ENTRY_NAME = 'app';
const VENDOR_FILE = './app/vendor.ts';
const BOOTSTRAP_FILE = './app/bootstrap.tsx';

module.exports = (env, argv) => {
    const config =  {
        context: path.resolve(__dirname, 'src'),
        entry: {
            [ENTRY_NAME]: [
                'webpack/hot/only-dev-server',
                VENDOR_FILE,
                BOOTSTRAP_FILE,
            ],
        },
        output: {
            filename: 'bundle.js',
            path: __dirname + '/dist'
        },

        devServer: {
            contentBase: path.join(__dirname, 'dist'),
            compress: true,
            port: 9000
        },

        devtool: 'source-map',

        resolve: {
            extensions: ['.ts', '.tsx', '.js', '.json', '.less', '.css']
        },

        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    loader: 'awesome-typescript-loader',
                },
                {
                    enforce: 'pre',
                    test: /\.js$/,
                    loader: 'source-map-loader',
                },
                {
                    test: /\.less$/,
                    use: [
                        'style-loader',
                        {
                            loader: 'css-loader',
                            options: {
                                modules: true,
                                localIdentName: '[local]',
                            },
                        },
                        {
                            loader: 'postcss-loader',
                            options: { sourceMap: true },
                        },
                        {
                            loader: 'less-loader',
                            options: {
                                paths: [
                                    path.resolve(__dirname, 'node_modules'),
                                    path.resolve(__dirname, 'src'),
                                ],
                            },
                        },
                    ]
                },
                {
                    test: /\.(jpg|jpeg|gif|png|svg)$/,
                    use: {
                        loader: 'url-loader',
                        options: {
                            limit: 1024,
                            name: '[name].[ext]',
                        },
                    },
                },
                {
                    test: /\.css$/,
                    include: [/node_modules/],
                    loaders: [
                        {
                            loader: 'css-loader',
                            options: {
                                modules: true,
                                localIdentName: '[local]',
                            },
                        },
                        {
                            loader: 'postcss-loader',
                            options: {
                                sourceMap: true,
                            },
                        },
                    ],
                }
            ]
        },

        optimization: {
            splitChunks: {
                cacheGroups: {
                    default: false,
                    commons: {
                        test: /[\\/]node_modules[\\/]/,
                        name: 'vendor',
                        chunks: 'all',
                    },
                }
            }
        },

        plugins: [
            new HtmlWebpackPlugin({
                filename: 'index.html',
                template: './assets/index.html',
                hash: true,
                cache: false,
                chunks: ['vendor', 'app'],
                chunksSortMode: 'manual',
                inject: 'body',
                minify: (argv.mode === 'production')
            }),
        ],
    };

    if (argv.mode !== 'production') {
        config.devServer = {
            port: '9000',
            host: '0.0.0.0',
            // proxy: {
            //     '/api/': 'localhost:8080',
            // },
            historyApiFallback: true,
            https: true,
            stats: {
                cached: false,
                exclude: [],
            },
            overlay: {
                warnings: false,
                errors: true,
            },
        };
        config.watchOptions = {
            poll: true,
        };
    }

    return config;
};
