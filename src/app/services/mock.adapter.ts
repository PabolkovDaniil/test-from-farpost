import axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

interface MockPostData {
    posts: any[],
    page: number,
}

const adapter = new MockAdapter(axios);

const dataByCid = new Map<string, MockPostData>();

function shuffle<T>(arr: T[]): T[] {
    return arr.sort((a, b) => {
        const ar = Math.random();
        const br = Math.random();
        return ar - br;
    });
}

async function pause(timeout: number = 300) {
    await new Promise(resolve => {
        const t = setTimeout(() => {
            clearTimeout(t);
            resolve();
        }, timeout);
    });
}

adapter
    .onPost('/login')
    .reply(async () => {
        await pause(1500);

        const cid = String(Date.now());
        const posts = require('./posts.mock.json');

        dataByCid.set(cid, {
            posts: shuffle(posts),
            page: 1,
        });
        return [200, { cid }];
    })
;

adapter
    .onGet('/posts')
    .reply(async (config) => {
        await pause();

        const cid = config.headers['Auth'];
        if (cid == null) {
            return [401];
        }

        const data = dataByCid.get(cid);
        const start = ((data.page - 1) * 10);
        const len = 10;
        const postsSlice = data.posts.slice(start, start + len);

        const page = data.page++;

        return [200, {
            page,
            count: postsSlice.length,
            theLast: (start + len >= data.posts.length),
            posts: postsSlice,
        }];
    })
;

adapter
    .onPost('/posts')
    .reply(async () => {
        await pause();

        return [200];
    })
;
