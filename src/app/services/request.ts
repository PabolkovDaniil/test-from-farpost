import axios, { AxiosInstance } from 'axios';
import { ResponseError } from '../utils/errors';

export interface IRequest {

    axios: AxiosInstance;

    get(url: string, params?: object): Promise<any>;
    post(url: string, body?: object): Promise<any>;

}

export class Request implements IRequest {

    public axios: AxiosInstance;

    constructor() {
        this.axios = axios;
    }

    async get(url: string, params: object = {}) {
        try {
            const response = await this.axios.get(url, {params});
            if (response.status != 200) {
                throw new ResponseError(response);
            }

            return response;
        } catch (err) {
            if (err instanceof ResponseError || err.response == null) {
                throw err;
            } else {
                throw new ResponseError(err.response);
            }
        }
    }

    async post(url: string, body: object = {}) {
        try {
            const response = await this.axios.post(url, body);
            if (response.status != 200) {
                throw new ResponseError(response);
            }

            return response;
        } catch (err) {
            if (err instanceof ResponseError || err.response == null) {
                throw err;
            } else {
                throw new ResponseError(err.response);
            }
        }
    }

    private static __request?: IRequest = null;

    static singleton(): IRequest {
        if (this.__request == null) {
            this.__request = new this();
        }

        return this.__request;
    }

}