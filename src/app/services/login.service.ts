import { Request } from './request';

export class LoginService {

    async login() {
        const response = await Request.singleton().post('login');
        const { cid } = response.data;
        Request.singleton().axios.defaults.headers.common['Auth'] = cid;
    }

}
