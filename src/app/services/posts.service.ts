import { Request } from './request';

export interface IPostRowData {
    // int ID объявления
    id: number;

    // int timestamp даты публикации
    publishDate: number;

    // string дата публикации в человекочитаемом формате
    publishDateString: string;

    // int ID владельца объявления
    ownerId: number;

    // string имя пользователя владельца объявления
    ownerLogin: string;

    // string Заголовок объявления
    bulletinSubject: string;

    // string Текст объявления
    bulletinText: string;

    // string[] ссылки на прикрепленные к объявлению изображения
    bulletinImages: string[];
}

export interface IPost extends IPostRowData {
    // Дествие модератора
    action?: PostAction;

    // Причина отклонения заявки
    declineDescription?: string;
}

export enum PostAction {
    Approve,
    Decline,
    Escalate,
}

export class Post implements IPost {

    public id: number;
    public publishDate: number;
    public publishDateString: string;
    public ownerId: number;
    public ownerLogin: string;
    public bulletinSubject: string;
    public bulletinText: string;
    public bulletinImages: string[];

    public action?: PostAction = null;
    public declineDescription?: string = null;

    constructor(data: IPostRowData) {
        this.id = data.id;
        this.publishDate = data.publishDate;
        this.publishDateString = data.publishDateString;
        this.ownerId = data.ownerId;
        this.ownerLogin = data.ownerLogin;
        this.bulletinSubject = data.bulletinSubject;
        this.bulletinText = data.bulletinText;
        this.bulletinImages = data.bulletinImages;
    }

}

export class PostsService {

    public posts: Post[] = [];
    public isLastPage: boolean = false;

    async get() {
        const { data } = await Request.singleton().get('/posts');
        this.isLastPage = data.theLast;
        this.posts = data.posts.map((postData: any) => new Post(postData));
    }

    async save(posts: Post[]) {
        await Request.singleton().post('/posts', posts.map(post => ({
            id: post.id,
            action: {
                [PostAction.Approve]: 'approve',
                [PostAction.Decline]: 'decline',
                [PostAction.Escalate]: 'escalate',
            }[post.action],
            descriptions: {
                decline: post.declineDescription,
            },
        })));
    }

}
