import 'react';
import 'react-dom';
import './services/mock.adapter';

import 'sanitize.css';
import '../assets/styles/icons.less';
import '../assets/styles/globals.less';
import '../assets/styles/modal.less';
import '../assets/styles/buttons.less';
