import * as React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom'

import { AppContainer } from './components/AppContainer';
import { EnterScreen } from './components/EnterScreen';
import { ModerateScreen } from './components/ModerateScreen';

export function AppRouter() {
    return (
        <BrowserRouter>
            <AppContainer>
                <Switch>
                    <Route path="/begin" component={EnterScreen} />
                    <Route exact path="/moderate" component={ModerateScreen} />

                    <Redirect path="*" to="/begin" />
                </Switch>
            </AppContainer>
        </BrowserRouter>
    );
}
