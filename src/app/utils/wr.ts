import * as ReactRouterDom from 'react-router-dom';

// Пришлось сделать так, чтобы использовать withRouter как декоратор, а не как функцию
export function withRouter(context: any): any {
    return ReactRouterDom.withRouter(context);
}