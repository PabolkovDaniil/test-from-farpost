import { AxiosResponse } from 'axios';

export class ResponseError {

    public response: AxiosResponse;
    public status: number;
    public statusText: string;

    constructor(response: AxiosResponse) {
        this.response = response;
        this.status = response.status;
        this.statusText = response.statusText;
    }

}