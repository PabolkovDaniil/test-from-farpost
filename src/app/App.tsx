import * as React from 'react';
import { AppRouter } from './AppRouter';

export interface AppProps {}

export class App extends React.Component<AppProps, {}> {
    render() {
        return (
            <div>
                <AppRouter />
            </div>
        );
    }
}