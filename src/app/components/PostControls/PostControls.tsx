import * as React from 'react';
import { HotKeyButton } from '../HotKeyButton';

import './PostControls.less';

export interface PostControlsProps {
    inactive?: boolean,
    onApprove: () => void,
    onDecline: () => void,
    onEscalate: () => void,
}

export class PostControls extends React.Component<PostControlsProps> {

    static defaultProps = {
        inactive: false,
    };

    handleApprove = () => {
        this.props.onApprove();
    };

    handleDecline = () => {
        this.props.onDecline();
    };

    handleEscalate = () => {
        this.props.onEscalate();
    };

    render() {
        return (
            <div className="post-controls">
                <HotKeyButton
                    className="post-controls__button post-controls__button_approve"
                    hotKey="Ctrl+Enter"
                    disabled={this.props.inactive}
                    onClick={this.handleApprove}
                >
                    Одобрить
                </HotKeyButton>
                <HotKeyButton
                    className="post-controls__button post-controls__button_decline"
                    hotKey="Ctrl+Backspace"
                    disabled={this.props.inactive}
                    onClick={this.handleDecline}
                >
                    Отклонить
                </HotKeyButton>
                <HotKeyButton
                    className="post-controls__button post-controls__button_escalate"
                    hotKey="Ctrl+Up"
                    disabled={this.props.inactive}
                    onClick={this.handleEscalate}
                >
                    Передать старшему модератору
                </HotKeyButton>
            </div>
        );
    }

}