import * as React from 'react';
import { PostAction } from '../../services/posts.service';
import { HotKeyButton } from '../HotKeyButton';

import './ResolvedPostControls.less';

export interface ResolvedPostControlsProps {
    action: PostAction,
    actionDescription?: string,
    onUndo: () => void,
}

export interface ResolvedPostControlsState {
    isDescriptionModalShown: boolean,
}

export class ResolvedPostControls extends React.Component<ResolvedPostControlsProps, ResolvedPostControlsState> {

    state = {
        isDescriptionModalShown: false,
    };

    handleUndo = () => {
        this.props.onUndo();
    };

    handleShowDescriptionModal = () => {
        this.setState({ isDescriptionModalShown: true });
    };

    handleCloseDescriptionModal = () => {
        this.setState({ isDescriptionModalShown: false });
    };

    renderDescriptionModal() {
        if (this.state.isDescriptionModalShown == false) {
            return null;
        }

        return (
            <div className="modal">
                <div className="modal__container">
                    <div
                        className="modal__backdrop back post-controls_resolved__modal__backdrop"
                        onClick={this.handleCloseDescriptionModal}
                    />

                    <div className="modal__content post-controls_resolved__modal__content">
                        <div className="modal__header">
                            <span>Причина отклонения</span>
                            <HotKeyButton
                                hotKey="Esc"
                                hideHotKey
                                title="Закрыть окно"
                                className="modal__header__close-button"
                                onClick={this.handleCloseDescriptionModal}
                            />
                        </div>
                        <div className="modal__body post-controls_resolved__modal__body">
                            <pre>
                                {this.props.actionDescription}
                            </pre>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderDescription() {
        if (this.props.action != PostAction.Decline) {
            return null;
        }

        return (
            <div className="post-controls__action-description">
                <div className="post-controls__action-description__sub">
                    <span className="post-controls__action-description__title">Описание:</span>
                    <span>{this.props.actionDescription}</span>
                </div>
                <HotKeyButton
                    className="button post-controls__action-description__show-more-button"
                    hotKey="Ctrl+Alt+F"
                    onClick={this.handleShowDescriptionModal}
                >
                    Показать полностью
                </HotKeyButton>
            </div>
        );
    }

    renderMark() {
        let actionTitle = null;
        let actionInfoClassName = null;

        switch (this.props.action) {
            case PostAction.Approve: {
                actionInfoClassName = 'post-controls__action-info_approved';
                actionTitle = 'Одобрено';
                break;
            }
            case PostAction.Decline: {
                actionInfoClassName = 'post-controls__action-info_declined';
                actionTitle = 'Отклонено';
                break;
            }
            case PostAction.Escalate: {
                actionInfoClassName = 'post-controls__action-info_escalated';
                actionTitle = 'Передано старшему моератору';
                break;
            }
        }

        return (
            <div className={`post-controls__action-info ${actionInfoClassName}`}>
                {actionTitle}
            </div>
        );
    }

    render() {
        return (
            <div className="post-controls post-controls_resolved">
                <div className="post-controls__action-info-container">
                    {this.renderMark()}
                    <HotKeyButton
                        className="button post-controls__action-info__change-button"
                        hotKey="Ctrl+Esc"
                        onClick={this.handleUndo}
                    >
                        Изменить
                    </HotKeyButton>
                </div>
                {this.renderDescription()}
                {this.renderDescriptionModal()}
            </div>
        );
    }
}