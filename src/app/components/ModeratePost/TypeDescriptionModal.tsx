import * as React from 'react';
import { HotKeyButton } from '../HotKeyButton';

import './TypeDescriptionModal.less';

export interface TypeDescriptionModalProps {
    onCancel: () => void,
    onSave: (description: string) => void,
}

export interface TypeDescriptionModalState {
    description: string,
}

export class TypeDescriptionModal extends React.Component<TypeDescriptionModalProps, TypeDescriptionModalState> {

    state = {
        description: '',
    };

    handleDescriptionChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        this.setState({ description: e.target.value });
    };

    handleSave = () => {
        this.props.onSave(this.state.description);
    };

    handleKeyUp = (e: React.KeyboardEvent<HTMLTextAreaElement>) => {
        if (e.ctrlKey && e.keyCode == 13) {
            e.preventDefault();
            e.stopPropagation();
            if (this.state.description.trim().length > 0) {
                this.handleSave();
            }
        }
    };

    render() {
        return (
            <div className="modal description-modal">
                <div className="modal__backdrop back description-modal__backdrop" />
                <div className="modal__container">
                    <div className="modal__content description-modal__content">
                        <div className="modal__header description-modal__header">
                            Причина отклонения
                            <HotKeyButton
                                className="modal__header__close-button"
                                hotKey="Esc"
                                hideHotKey
                                title="Закрыть окно"
                                onClick={this.props.onCancel}
                            />
                        </div>
                        <div className="modal__body">
                            <textarea
                                rows={6}
                                autoFocus
                                className="description-modal__textarea"
                                placeholder="Укажите (в свободной форме) почему Вы отклониле данный пост"
                                value={this.state.description}
                                onKeyUp={this.handleKeyUp}
                                onChange={this.handleDescriptionChange}
                            />
                        </div>
                        <div className="modal__footer description-modal__footer">
                            <button
                                className="description-modal__button"
                                onClick={this.props.onCancel}
                            >
                                Отмена
                            </button>
                            <button
                                className="description-modal__button"
                                disabled={this.state.description.trim().length == 0}
                                onClick={this.handleSave}
                            >
                                Сохранить
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}