import * as React from 'react';
import {Post, PostAction} from '../../services/posts.service';

import './ModeratePost.less';
import {PostItem} from "../PostItem";
import {PostControls, ResolvedPostControls} from "../PostControls";
import {TypeDescriptionModal} from "./TypeDescriptionModal";

export interface ModeratePostProps {
    post: Post,
    onResolved: (postId: number, action: PostAction|null) => void,
}

interface ModeratePostState {
    isTypeDeclineReasonModalShown: boolean,
}

export class ModeratePost extends React.Component<ModeratePostProps, ModeratePostState> {

    state = {
        isTypeDeclineReasonModalShown: false,
    };

    handleUnresolvePost = () => {
        this.props.post.action = null;
        // this.forceUpdate();
        this.props.onResolved(this.props.post.id, this.props.post.action);
    };

    handleApprovePost = () => {
        this.props.post.action = PostAction.Approve;
        // this.forceUpdate();
        this.props.onResolved(this.props.post.id, this.props.post.action);
    };

    handleDeclinePost = () => {
        this.setState({
            isTypeDeclineReasonModalShown: true,
        });
    };

    handleEscalatePost = () => {
        this.props.post.action = PostAction.Escalate;
        this.props.onResolved(this.props.post.id, this.props.post.action);
    };

    handleSaveDeclineReason = (text: string) => {
        this.props.post.action = PostAction.Decline;
        this.props.post.declineDescription = text;
        this.props.onResolved(this.props.post.id, this.props.post.action);
        this.handleCloseTypeDeclineReasonModal();
    };

    handleCloseTypeDeclineReasonModal = () => {
        this.setState({ isTypeDeclineReasonModalShown: false });
    };

    renderPostControls() {
        let controls = null;
        if (this.props.post.action == null) {
            controls = (
                <PostControls
                    key={this.props.post.id}
                    inactive={this.state.isTypeDeclineReasonModalShown}
                    onApprove={this.handleApprovePost}
                    onDecline={this.handleDeclinePost}
                    onEscalate={this.handleEscalatePost}
                />
            );
        } else {
            controls = (
                <ResolvedPostControls
                    key={this.props.post.id}
                    action={this.props.post.action}
                    actionDescription={this.props.post.declineDescription}
                    onUndo={this.handleUnresolvePost}
                />
            );
        }

        return (
            <div className="moderate-screen__post-controls">
                <div className="moderate-screen__post-controls__container">
                    {controls}
                </div>
            </div>
        );
    }

    renderTypeDeclineReasonModal() {
        if (this.state.isTypeDeclineReasonModalShown == false) {
            return null;
        }

        return (
            <TypeDescriptionModal
                onCancel={this.handleCloseTypeDeclineReasonModal}
                onSave={this.handleSaveDeclineReason}
            />
        );
    }

    render() {
        return (
            <div className="moderate-post">
                {this.renderPostControls()}
                <div className="moderate-screen__content">
                    <div className="moderate-screen__content__sub">
                        <div className="moderate-screen__posts">
                            <PostItem post={this.props.post} />
                        </div>
                    </div>
                </div>
                {this.renderTypeDeclineReasonModal()}
            </div>
        );
    }

}