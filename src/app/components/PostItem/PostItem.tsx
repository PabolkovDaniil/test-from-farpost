import * as React from 'react';
import { IPost } from '../../services/posts.service';

import './PostItem.less';

export interface PostItemProps {
    post: IPost,
}

export class PostItem extends React.Component<PostItemProps> {

    render() {
        return (
            <div className="post-item">
                <div className="post-item__header">
                    <div className="post-item__header__left-container">
                        <div className="post-item__post-info">
                            <span className="post-item__post-info__id">{this.props.post.id}</span>
                            <span> &mdash; </span>
                            <span>{this.props.post.publishDateString}</span>
                        </div>
                    </div>
                    <div className="post-item__header__right-container">
                        <a
                            href={`https://www.farpost.ru/user/${this.props.post.ownerId}`}
                            className="post-item__author"
                            target="_blank"
                            referrerPolicy="no-referrer"
                        >
                            <span className="user-icon post-item__author__icon" />
                            <span>{this.props.post.ownerLogin}</span>
                        </a>
                    </div>
                </div>
                <div className="post-item__body">
                    <div className="post-item__title">
                        {this.props.post.bulletinSubject}
                    </div>
                    <div className="post-item__content">
                        <div className="post-item__attachments">
                            <div className="post-item__empty-row">Нет ни одного прикреплённого изображения</div>
                            {this.props.post.bulletinImages.map(link => (
                                <div key={link} className="post-item__attachment-item">
                                    <a
                                        href={link}
                                        className="post-item__attachment-item__image-container"
                                        target="_blank"
                                    >
                                        <img className="post-item__attachment-item__image" src={link} alt={link} />
                                    </a>
                                </div>
                            ))}
                        </div>
                        <div className="post-item__text">
                            {this.props.post.bulletinText}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}