import * as React from 'react';

import './LastScreen.less';

export interface LastScreenProps {}

export class LastScreen extends React.Component<LastScreenProps> {

    render() {
        return (
            <div className="last-screen">
                <div className="last-screen__content">
                    <div>Спасибо!</div>
                    <div>Больше нет постов для модерации.</div>
                </div>
            </div>
        );
    }

}