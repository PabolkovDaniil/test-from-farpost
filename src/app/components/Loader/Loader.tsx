import * as React from 'react';

import './Loader.less';

export interface LoaderProps extends React.HTMLProps<HTMLDivElement> {}

export class Loader extends React.Component<LoaderProps> {

    render() {
        const className = this.props.className;
        return (
            <div {...this.props} className={`loader ${className || ''}`}>
                <div className="loader__content">{this.props.children}</div>
            </div>
        );
    }

}