import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { Loader } from "../Loader";
import { LoginService } from '../../services/login.service';
import { withRouter } from '../../utils/wr';

import './EnterScreen.less';

export interface EnterScreenProps extends RouteComponentProps {}

interface EnterScreenState {
    isLoading: boolean,
    authError: Error,
}

@withRouter
export class EnterScreen extends React.Component<EnterScreenProps, EnterScreenState> {

    private loginService: LoginService;

    state: EnterScreenState = {
        isLoading: false,
        authError: null,
    };

    componentWillMount(): void {
        this.loginService = new LoginService();
    }

    componentDidMount(): void {
        this.keyBind();
    }

    componentWillUnmount(): void {
        this.keyUnbind();
    }

    keyBind = () => {
        window.addEventListener('keydown', this.handleKeyDown);
    };

    keyUnbind = () => {
        window.removeEventListener('keydown', this.handleKeyDown);
    };

    handleKeyDown = (e: any) => {
        if (e.keyCode == 13) {
            this.beginWork();
        }
    };

    async beginWork() {
        this.setState({ isLoading: true });
        try {
            await this.loginService.login();
            this.props.history.push('/moderate');
        } catch (err) {
            this.setState({
                authError: err,
                isLoading: false,
            });
        }
    }

    handleEnterClick = () => {
        this.beginWork();
    };

    renderErrorForm() {
        return (
            <div className="enter-info">
                <div className="enter-info__logo enter-info__logo_error" onClick={this.handleEnterClick}>
                    <span className="enter-info__logo__text">Enter</span>
                </div>
                <div className="enter-info__helper enter-info__helper_error">
                    <div>Произошла ошибка авторизации</div>
                    <div>Нажмите &laquo;Enter&raquo; чтобы попробовать снова</div>
                </div>
            </div>
        );
    }

    renderLoadingForm() {
        return (
            <div className="enter-info">
                <Loader>Авторизация</Loader>
            </div>
        );
    }

    renderEnterForm() {
        return (
            <div className="enter-info">
                <div className="enter-info__logo" onClick={this.handleEnterClick}>
                    <span className="enter-info__logo__text">Enter</span>
                </div>
                <div className="enter-info__helper">Нажмите &laquo;Enter&raquo; чтобы начать</div>
            </div>
        )
    }

    renderContent() {
        if (this.state.isLoading) {
            return this.renderLoadingForm();
        }

        if (this.state.authError != null) {
            return this.renderErrorForm();
        }

        return this.renderEnterForm();
    }

    render() {
        return (
            <div className="enter-screen">
                {this.renderContent()}
            </div>
        );
    }
}
