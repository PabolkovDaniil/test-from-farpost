import * as React from 'react';

import './AppContainer.less';

export interface AppContainerProps {}

export class AppContainer extends React.Component<AppContainerProps> {

    render() {
        return (
            <div {...this.props} className="app-background back">
                <div className="app-background__content">
                    {this.props.children}
                </div>
            </div>
        );
    }

}
