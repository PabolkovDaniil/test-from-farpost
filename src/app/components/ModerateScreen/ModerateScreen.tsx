import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { Loader } from '../Loader';
import { PostAction, PostsService } from '../../services/posts.service';
import { withRouter } from '../../utils/wr';
import { ResponseError } from '../../utils/errors';
import { NavigationPanel } from '../NavigationPanel';
import { ModeratePost } from "../ModeratePost";
import { LastScreen } from "../LastScreen";

import './ModerateScreen.less';

export interface ModerateScreenProps extends RouteComponentProps {}

export interface ModerateScreenState {
    isLoading: boolean,
    isSaving: boolean,
    loadingError?: Error,
    savingError?: Error,
    activePostIndex: number,
    activePostId: number,
    postsActions: Map<number, { action: PostAction, description?: string }>,
    declineDescriptionModalPostId?: number,
}

@withRouter
export class ModerateScreen extends React.Component<ModerateScreenProps, ModerateScreenState> {

    private postsService: PostsService = new PostsService();

    state: ModerateScreenState = {
        isLoading: true,
        isSaving: false,
        loadingError: null,
        savingError: null,
        activePostIndex: 0,
        activePostId: null,
        postsActions: new Map(),
        declineDescriptionModalPostId: null,
    };

    componentDidMount(): void {
        this.init();
    }

    async init() {
        this.setState({ isLoading: true });
        const nextState = await this.loadPosts();
        if (nextState != null) {
            this.state.postsActions.clear();
            this.setState({
                ...nextState,
                isLoading: false,
            });
        }
    }

    async loadPosts(): Promise<ModerateScreenState|null> {
        const nextState: ModerateScreenState = {...this.state};
        try {
            await this.postsService.get();

            nextState.activePostIndex = 0;
            nextState.activePostId = this.postsService.posts[nextState.activePostIndex].id;
            nextState.postsActions.clear();
            nextState.declineDescriptionModalPostId = null;
            nextState.loadingError = null;

            return nextState;
        } catch (error) {
            if (error instanceof ResponseError) {
                if (error.status == 401) {
                    this.props.history.replace('/begin');
                    return void 0;
                }
            }

            nextState.loadingError = error;

            return nextState;
        }
    }

    handleChangeActivePost = (postId: number) => {
        const selectedPost = this.postsService.posts.find(post => post.id == postId);
        this.setState({
            activePostIndex: this.postsService.posts.indexOf(selectedPost),
        });
    };

    handleResolvePost = (postId: number, action: PostAction|null) => {
        if (action != null) {
            this.setState({
                activePostIndex: Math.min(this.state.activePostIndex + 1, this.postsService.posts.length - 1),
            });
        } else {
            this.forceUpdate();
        }
    };

    handleNext = async () => {
        this.setState({ isSaving: true });
        try {
            await this.postsService.save(this.postsService.posts);
        } catch (err) {
            this.setState({
                isSaving: false,
                savingError: err,
            });
            return;
        }

        const nextState = await this.loadPosts();

        this.setState({
            ...nextState,
            isSaving: false,
        });
    };

    handleRetrySaveNow = () => {
        this.setState({
            savingError: null
        }, () => {
            this.handleNext();
        });
    };

    handleRetrySaveLater = () => {
        this.setState({
            savingError: null,
        });
    };

    handleRetryLoadNow = () => {
        this.setState({
            loadingError: null,
        }, async () => {
            await this.init();
        });
    };

    handleRetryLoadLater = () => {
        this.setState({
            loadingError: null,
        });
    };

    renderLoadIndicator() {
        return (
            <div className="loader-container">
                <div className="back loader-container__back" />
                <Loader className="loader-container__container">Загрузка</Loader>
            </div>
        );
    }

    renderSaveIndicator() {
        return (
            <div className="loader-container">
                <div className="back loader-container__back" />
                <Loader className="loader-container__container">Сохранение</Loader>
            </div>
        );
    }

    renderSavingError() {
        return (
            <div className="loader-container">
                <div className="back loader-container__back" />
                <div className="loader-container__container moderate-screen__error">
                    <div className="moderate-screen__error__logo">
                        Ошибка
                    </div>
                    <div className="enter-info__helper enter-info__helper_error">
                        <div>Не удалось сохранить результат модерации</div>
                        <div>
                            <button
                                className="button moderate-screen__error__button"
                                onClick={this.handleRetrySaveLater}
                            >
                                Попробовать позже
                            </button>
                            <button
                                className="button moderate-screen__error__button"
                                onClick={this.handleRetrySaveNow}
                            >
                                Повторить сейчас
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderLoadingError() {
        return (
            <div className="loader-container">
                <div className="back loader-container__back" />
                <div className="loader-container__container moderate-screen__error">
                    <div className="moderate-screen__error__logo">
                        Ошибка
                    </div>
                    <div className="enter-info__helper enter-info__helper_error">
                        <div>Не удалось загрузить посты для модерации</div>
                        <div>
                            <button
                                className="button moderate-screen__error__button"
                                onClick={this.handleRetryLoadLater}
                            >
                                Попробовать позже
                            </button>
                            <button
                                className="button moderate-screen__error__button"
                                onClick={this.handleRetryLoadNow}
                            >
                                Повторить сейчас
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    renderNavigationPanel() {
        if (this.state.isLoading && this.postsService.posts.length == 0) {
            return void 0;
        }

        return (
            <div className="moderate-screen__navigation-panel">
                <NavigationPanel
                    items={this.postsService.posts.map(post => {
                        return {
                            id: post.id,
                            title: post.bulletinSubject,
                            action: post.action,
                            description: post.declineDescription,
                        };
                    })}
                    activeItemId={this.postsService.posts[this.state.activePostIndex].id}
                    onChoose={this.handleChangeActivePost}
                    onNext={this.handleNext}
                />
            </div>
        );
    }

    renderContentLoader() {
        return (
            <div className="moderate-screen">
                {this.renderLoadIndicator()}
            </div>
        );
    }

    renderContent() {
        if (this.postsService.posts.length == 0) {
            return (<LastScreen />);
        }

        const post = this.postsService.posts[this.state.activePostIndex];
        return (
            <div className="moderate-screen">
                <ModeratePost
                    key={post.id}
                    post={post}
                    onResolved={this.handleResolvePost}
                />
                {this.renderNavigationPanel()}

                {this.state.isLoading ? this.renderLoadIndicator() : null}
                {this.state.isSaving ? this.renderSaveIndicator() : null}
                {this.state.savingError ? this.renderSavingError() : null}
                {this.state.loadingError ? this.renderLoadingError() : null}
            </div>
        );
    }

    render() {
        return (
            this.state.isLoading && this.postsService.posts.length == 0
                ? this.renderContentLoader()
                : this.renderContent()
        );
    }
}