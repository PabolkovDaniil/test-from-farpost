import * as React from 'react';
import { HotKeyButton } from '../HotKeyButton';
import { PostAction } from '../../services/posts.service';

import './NavigationPanel.less';

export interface NavigationPanelProps {
    items: {
        id: number,
        title: string,
        action?: PostAction,
        description?: string,
    }[],
    activeItemId: number,
    onChoose: (itemId: number) => void,
    onNext: () => void,
}

export class NavigationPanel extends React.Component<NavigationPanelProps> {

    private handleChoose = (e: React.MouseEvent<HTMLButtonElement>) => {
        const selectedId: number = Number(e.currentTarget.dataset.id);
        this.props.onChoose(selectedId);

    };

    private handleNext = () => {
        this.props.onNext();
    };

    private hotKeyFormatter = (hotKeyTitle: string) => {
        return hotKeyTitle;
    };

    private getPostClassName(action: PostAction): string {
        let className = 'nav-panel__icon';
        switch (action) {
            case PostAction.Approve: {
                className += ' nav-panel__icon_approved';
                break;
            }
            case PostAction.Decline: {
                className += ' nav-panel__icon_declined';
                break;
            }
            case PostAction.Escalate: {
                className += ' nav-panel__icon_escalated';
                break;
            }
            default: {
                className += ' nav-panel__icon_unchecked';
                break;
            }
        }

        return className;
    }

    render() {
        const allIsResolved = this.props.items.reduce((res, x) => Boolean(res && x.action != null), true);
        return (
            <div className="nav-panel">
                {
                    this.props.items.map((item, index) => {
                        let key = (index + 1);
                        if (key == 10) {
                            key = 0;
                        } else if (key > 10) {
                            key = null;
                        }

                        let hotKey = null;
                        if (key != null) {
                            hotKey = [`Ctrl+${key}`];
                        }

                        return (
                            <HotKeyButton
                                key={item.id}
                                hotKey={hotKey}
                                hotKeyClassName="nav-panel__item__hot-key-title"
                                hotKeyFormatter={this.hotKeyFormatter}
                                className={`nav-panel__item ${item.id == this.props.activeItemId ? 'nav-panel__item_active' : ''}`}
                                title={item.title}
                                onClick={this.handleChoose}
                                {...{['data-id']: item.id}}
                            >
                                <div className="nav-panel__item__content">
                                    <span className={this.getPostClassName(item.action)}/>
                                    <span className="nav-panel__title">{item.title}</span>
                                </div>
                            </HotKeyButton>
                        );
                    })
                }
                <HotKeyButton
                    hotKey="F7"
                    hotKeyClassName="nav-panel__item__hot-key-title"
                    hotKeyFormatter={this.hotKeyFormatter}
                    className="nav-panel__item"
                    disabled={!allIsResolved}
                    onClick={this.handleNext}
                >
                    <div className="nav-panel__item__content">
                        <span className="nav-panel__icon nav-panel__icon_next" />
                        <span className="nav-panel__title">Далее</span>
                    </div>
                </HotKeyButton>
            </div>
        );
    }
}