import * as React from 'react';
const keycomb = require('keycomb');

export interface HotKeyButtonProps {
    // Комбинация клавиш, разделённых плюсом (+)
    hotKey: string|string[],
    hideHotKey?: boolean,
    hotKeyClassName?: string,
    hotKeyFormatter?: (hotKeyTitle: string) => string,
    onClick: (e?: any) => void;
}

type Props = (HotKeyButtonProps & React.ButtonHTMLAttributes<HTMLButtonElement>);

const TitleAliases: { [key: string]: string } = {
    'up': '↑',
    'down': '↓',
    'left': '←',
    'right': '→',
    'enter': '↵',
    'backspace': 'Backspace',
    'delete': 'Del',
    'escape': 'Esc',
};

type TKeyComb = {
    altKey: boolean,
    ctrlKey: boolean,
    shiftKey: boolean,
    keyCode: number[],
}

export class HotKeyButton extends React.Component<Props> {

    static defaultProps = {
        hideHotKey: false,
    };

    private hotkeyTitle: string;
    private pressedKeys: Set<number> = new Set();
    private rules: Map<string, TKeyComb> = new Map();

    private $button: HTMLButtonElement;

    componentWillMount() {
        this.prepareHokey();
    }

    componentWillUnmount(): void {
        this.unbindHotKey();
    }

    prepareHokey() {
        let hotKeys = [];
        if (this.props.hotKey instanceof Array) {
            hotKeys.push(...this.props.hotKey);
        } else {
            hotKeys.push(this.props.hotKey);
        }

        this.pressedKeys = new Set();
        this.rules = new Map();

        this.hotkeyTitle = hotKeys.join(', ');
        hotKeys.forEach(hotKey => {
            this.rules.set(hotKey, keycomb(hotKey));
        });

        this.bindHotKey();
    }

    componentWillReceiveProps(nextProps: Readonly<Props>, nextContext: any): void {
        if (nextProps.disabled != this.props.disabled) {
            if (nextProps.disabled == true ){
                this.unbindHotKey();
            } else {
                this.bindHotKey();
            }
        }

        if (nextProps.hotKey != this.props.hotKey) {
            this.unbindHotKey();
            this.prepareHokey();
        }
    }

    bindHotKey() {
        window.addEventListener('keydown', this.handleGlobalKeyDown);
        window.addEventListener('keyup', this.handleGlobalKeyUp);
    }

    unbindHotKey() {
        window.removeEventListener('keydown', this.handleGlobalKeyDown);
        window.removeEventListener('keyup', this.handleGlobalKeyUp);
    }

    handleGlobalKeyDown = (e: KeyboardEvent) => {
        /**
         * Не добавляем в список нажатых клавиш следующие:
         * 16 - Shift
         * 17 - Ctrl
         * 18 - Alt
         * т.е. они определены своими булевыми переменными (в KeyboardEvent и TKeyComb)
         */
        if (![16, 17, 18].includes(e.keyCode)) {
            this.pressedKeys.add(e.keyCode);
        }

        this.rules.forEach(rule => {
            const allKeysIsPressed = (
                this.pressedKeys.size == rule.keyCode.length
                && rule.keyCode.reduce((res, keyCode) => {
                    return (res && this.pressedKeys.has(keyCode));
                }, true)
            );

            if (
                e.ctrlKey == rule.ctrlKey
                && e.altKey == rule.altKey
                && e.shiftKey == rule.shiftKey
                && allKeysIsPressed
            ) {
                e.preventDefault();
                this.$button.click();
            }
        });
    };

    handleGlobalKeyUp = (e: KeyboardEvent) => {
        this.pressedKeys.delete(e.keyCode);
    };

    getHotKeyToText(): string {
        return this.hotkeyTitle
            .split('+')
            .map(x => TitleAliases[x.toLowerCase()] || x)
            .join('+')
        ;
    }

    getTitle(): string {
        return `${this.props.title || ''} (${this.getHotKeyToText()})`.trim();
    }

    defaultHotKeyFormatter = (hotKeyTitle: string) => {
        return `(${hotKeyTitle})`;
    };

    renderHotKey() {
        if (this.props.hideHotKey) {
            return null;
        }

        return (
            <span className={this.props.hotKeyClassName}>
                {(this.props.hotKeyFormatter || this.defaultHotKeyFormatter)(this.getHotKeyToText())}
            </span>
        );
    }

    render() {
        const { hotKey, hideHotKey, hotKeyClassName, hotKeyFormatter, ...props } = this.props;
        return (
            <button
                ref={$el => this.$button = $el}
                type="button"
                {...props}
                title={this.getTitle()}
            >
                {this.props.children} {this.renderHotKey()}
            </button>
        );
    }
}
